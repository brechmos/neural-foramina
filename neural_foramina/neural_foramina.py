"""Main module."""
import glob
import matplotlib.pyplot as plt
import pydicom
from pydicom import dcmread
from pydicom.dataset import Dataset
import pandas as pd
import numpy as np
from pathlib import Path
from pylab import imshow, show, get_cmap
from scipy.interpolate import CubicSpline, interp1d
from mpl_toolkits.mplot3d import Axes3D
import scipy.ndimage
from scipy.ndimage import map_coordinates

import scipy as sp
import scipy.interpolate


class SplayedImage:

    def __init__(self):
        pass

    def create_splayed_image(self, file):

        # Group annotated points into sets of 3 (left point, middle, right point)
        ct_data, pts = self.get_slices(file)

        # Points per slice (red dots in screen shot)
        pts_from_file =  self.group_pts(file)

        pts_per_slice = self.interpolate_points_per_side(pts_from_file)

        index_map = self.interpolate_points_per_slice(pts_per_slice)

        spl_image = self.interpolate_splayed_image(ct_data, index_map)

        return spl_image

    def get_reference_filename(self, dcm_file):
        """
        Given the filename with the points, get the reference (image) filename.
        """

        hdr = dcmread(dcm_file)
        ref_image = hdr[0x0008, 0x1115][0][0x0008, 0x1140][0][0x0008, 0x1155] # gets reference file number
        parent_path = Path(dcm_file).parent.parent
        filename = list(parent_path.glob('**/CT.{}.dcm'.format(ref_image.value)))[0] # parent directory of CT image

        return filename

    def load_points(self, dcm_file):
        """
        Load the Graphic Data Points from the file.
        """
        hdr = dcmread(dcm_file)

        if not [0x0070, 0x0001] in hdr:
            raise('First level not in header')
        elif [0x0070,0x0009] not in hdr[0x00070,0x001][0]:
            raise('Third level not in header')

        points = []
        for seq in hdr[0x0070, 0x0001][0][0x0070, 0x0009]: # seq under
            gd = seq[0x0070, 0x0022] # gets each graphic data points
            points.append(gd.value) # add graphic data point to points array

        return points


    def get_slices(self, dcm_file):
        """
        Load the image data and slice number of annotations.
        """

        #
        # Load the data.
        #

        # Get all the filenames of the points files
        filenames = sorted(glob.glob(dcm_file + '/*dcm'))

        # Get an image file
        first_image = self.get_reference_filename(filenames[0])
        directory_images = Path(first_image).parent

        # Create a dictionary that will let you lookup the “slice number’ from the filename
        slice_lookup = {}
        data = []
        for number, imfile in enumerate(sorted(directory_images.glob('*dcm'))):
            slice_lookup[imfile.name] = number # e.g., slice_lookup[‘CT.1.2.2389023948023.23.42.34.234.dcm’]  -> 12
            im = dcmread(imfile)
            # print(im[0x0020, 0x1041])
            data.append(im.pixel_array)  #  <- put the image into the data list

        # Convert the data list to a 3D numpy array
        data = np.stack(data, axis=2)

        #
        # Load the points
        #
        pts = []

        for filename in filenames: # for each point filename
            ref_points = self.load_points(filename)
            image_file = self.get_reference_filename(filename) # get the image filename

            slice = slice_lookup[image_file.name]

            for p in ref_points:
                pts.append([p[0], p[1], slice])

        return data, pts

    def _interp3(self, x, y, z, v, xi, yi, zi, **kwargs):
        """Sample a 3D array "v" with pixel corner locations at "x","y","z" at the
        points in "xi", "yi", "zi" using linear interpolation. Additional kwargs
        are passed on to ``scipy.ndimage.map_coordinates``."""
        def index_coords(corner_locs, interp_locs):
            index = np.arange(len(corner_locs))
            if np.all(np.diff(corner_locs) < 0):
                corner_locs, index = corner_locs[::-1], index[::-1]
            return np.interp(interp_locs, corner_locs, index)

        orig_shape = np.asarray(xi).shape
        xi, yi, zi = np.atleast_1d(xi, yi, zi)
        for arr in [xi, yi, zi]:
            arr.shape = -1

        output = np.empty(xi.shape, dtype=float)
        coords = [index_coords(*item) for item in zip([x, y, z], [xi, yi, zi])]

        map_coordinates(v, coords, order=1, output=output, **kwargs)

        return output.reshape(orig_shape)

    def group_pts(self, dcm_file):
        """
        Group points
        """
        n = 0
        points_list = []
        data, pts = self.get_slices(dcm_file)
        for i in range(0, len(pts), 3):
            points_list.append([[pts[i][0], pts[i][1], pts[i][2]], [pts[i+1][0], pts[i+1][1], pts[i+1][2]], [pts[i+2][0], pts[i+2][1], pts[i+2][2]]])

        return np.array(points_list)

    def interpolate_points_per_side(self, pts_from_file):
        """
        Interpolate the points along the left pedicles, mid-vertebral bodies,
        and right pedicles.
        """

        pts_per_slice = []

        sli = np.arange(100, 350)

        # left points
        left_pts = pts_from_file[:,0,:]
        # print(left_pts)

        sl = left_pts[:,2]
        x = left_pts[:,0]
        cs = CubicSpline(sl, x)
        xi_left = cs(sli)

        y = left_pts[:,1]
        cs = CubicSpline(sl, y)
        yi_left = cs(sli)


        # middle points
        mid_pts = pts_from_file[:,1,:]
        # print(mid_pts)

        sl = mid_pts[:,2]
        x = mid_pts[:,0]
        cs = CubicSpline(sl, x)
        xi_mid = cs(sli)

        y = mid_pts[:,1]
        cs = CubicSpline(sl, y)
        yi_mid = cs(sli)


        # right points
        right_pts = pts_from_file[:,2,:]
        # print(right_pts)

        sl = right_pts[:,2]
        x = right_pts[:,0]
        cs = CubicSpline(sl, x)
        xi_right = cs(sli)

        y = right_pts[:,1]
        cs = CubicSpline(sl, y)
        yi_right = cs(sli)

        for xxl, yyl, xxm, yym, xxr, yyr, zz in zip(xi_left, yi_left, xi_mid, yi_mid, xi_right, yi_right, sli):
            pts_per_slice.append([[xxl, yyl, zz], [xxm, yym, zz], [xxr, yyr, zz]])


        return pts_per_slice


    def interpolate_points_per_slice(self, pts_per_slice):

        # Calculate all index points we want to interpolate the signal intensity over
        xs = np.arange(500)
        index_map = []

        for s in range(len(pts_per_slice)):

            lp, mv, rp = pts_per_slice[s]

            # get the x and y coordinates of the pedicles and mid-vertebral bodies
            x = [lp[0], mv[0], rp[0]]
            y = [lp[1], mv[1], rp[1]]

            cs = CubicSpline(x, y)
            ys = cs(xs)

            for x, y in zip(xs, ys):
                index_map.append((x, y, lp[2]))

        index_map = np.array(index_map)

        return index_map


    def interpolate_splayed_image(self, ct_data, index_map):

        # Calculate the signal intensity from the 3D CT data at each of the points in the index_map
        # ct_data is the variable that contains the CT data (3d numpy array)

        x = np.linspace(0, ct_data.shape[0]-1, ct_data.shape[0])
        y = np.linspace(0, ct_data.shape[1]-1, ct_data.shape[1])
        z = np.linspace(0, ct_data.shape[2]-1, ct_data.shape[2])

        xit = index_map[:,1]
        yit = index_map[:,0]
        zit = index_map[:,2]

        # Interpolate a region of the x-y plane at z=-25
        splayed_image_almost = self._interp3(x, y, z, ct_data, xit, yit, zit)

        spl_image = splayed_image_almost.reshape((250, 500))

        return spl_image

