"""Top-level package for Neural Foramina."""

__author__ = """Craig Jones"""
__email__ = 'craig@brechmos.org'
__version__ = '0.1.0'

from .neural_foramina import *
