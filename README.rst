===============
Neural Foramina
===============


.. image:: https://img.shields.io/pypi/v/neural_foramina.svg
        :target: https://pypi.python.org/pypi/neural_foramina

.. image:: https://img.shields.io/travis/brechmos/neural_foramina.svg
        :target: https://travis-ci.com/brechmos/neural_foramina

.. image:: https://readthedocs.org/projects/neural-foramina/badge/?version=latest
        :target: https://neural-foramina.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Neural foramina work.


* Free software: BSD license
* Documentation: https://neural-foramina.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
