=====
Usage
=====

To use Neural Foramina in a project::

    import neural_foramina

```
splayed_image = SplayedImage()

spl_image = splayed_image.create_splayed_image(filename)
```

where `filename` is the dicom file that contains the points.
